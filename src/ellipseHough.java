
import java.awt.*;
import java.awt.image.*;
import java.applet.*;
import java.net.*;
import java.io.*;
import java.lang.Math;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JApplet;
import javax.imageio.*;
import javax.swing.event.*;

public class ellipseHough {

		ImagePerso input;
		//int[] input;
		ImagePerso output;
		ImagePerso EllipseOnly;
		float[] template={-1,0,1,-2,0,2,-1,0,1};;
		int width;
		int height;
		int[] acc;
		int accSize=1;  //nombre ellipse
		int[] results;
		int r;
		int r1;
		Vector2 centre;

		public void ellipseHough() {
			
		}

		public void init(ImagePerso inputIn, int widthIn, int heightIn, int radius, int radius1) {
			r = radius;
			r1 = radius1;
			this.width=widthIn;
			this.height=heightIn;
			//input = new int[width*height];
			//output = new int[width*height];
			this.input = inputIn;
			this.output = new ImagePerso(inputIn);
			this.EllipseOnly = new ImagePerso(inputIn);
			for(int i=0; i<this.input.getHeight();i++){  //on l'initialise en noir
				for(int j=0; j<this.input.getWidth();j++){
					this.EllipseOnly.img[i][j][0] = 0;
					this.EllipseOnly.img[i][j][1] = 0;
					this.EllipseOnly.img[i][j][2] = 0;
				}
			}

		}
		public void setLines(int lines) {
			accSize=lines;		
		}
		// hough transform for lines (polar), returns the accumulator array
		public ImagePerso process() {
	
			// for polar we need accumulator of 180degress * the longest length in the image
			int rmax = (int)Math.sqrt(width*width + height*height);
			acc = new int[width * height];
			for(int x=0;x<width;x++) {
				for(int y=0;y<height;y++) {
					acc[y*width+x] =0 ;
				}
			}			
			int x0, y0;
			double t;
				
			for(int x=0;x<width;x++) {		
				for(int y=0;y<height;y++) {
				
					if (input.img[y][x][0] == 255){
					
						for (int theta=0; theta<360; theta++) {
							t = (theta * 3.14159265) / 180;
							x0 = (int)Math.round(x - r * Math.cos(t));
							y0 = (int)Math.round(y - r1 * Math.sin(t));
							if(x0 < width && x0 > 0 && y0 < height && y0 > 0) {
								acc[x0 + (y0 * width)] += 1;
							}
						}
					}
				}
			}
		
			// now normalise to 255 and put in format for a pixel array
			int max=0;
		
			// Find max acc value
			for(int x=0;x<width;x++) {
				for(int y=0;y<height;y++) {

					if (acc[x + (y * width)] > max) {
						max = acc[x + (y * width)];
					}
				}
			}
		
			//System.out.println("Max :" + max);
		

			findMaxima();

			System.out.println("done");
			return output;
		}
		private void findMaxima() {
			results = new int[accSize*3];
		
			for(int x=0;x<width;x++) {
				for(int y=0;y<height;y++) {
					int value = (acc[x + (y * width)]);

					// if its higher than lowest value add it and then sort
					if (value > results[(accSize-1)*3]) {

						// add to bottom of array
						results[(accSize-1)*3] = value;
						results[(accSize-1)*3+1] = x;
						results[(accSize-1)*3+2] = y;
					
						// shift up until its in right place
						int i = (accSize-2)*3;
						while ((i >= 0) && (results[i+3] > results[i])) {
							for(int j=0; j<3; j++) {
								int temp = results[i+j];
								results[i+j] = results[i+3+j];
								results[i+3+j] = temp;
							}
							i = i - 3;
							if (i < 0) break;
						}
					}
				}
			}
		
			for(int i=accSize-1; i>=0; i--){			
				//System.out.println("value: " + results[i*3] + ", r: " + results[i*3+1] + ", theta: " + results[i*3+2]);
				drawEllipse(results[i*3], results[i*3+1], results[i*3+2]);
				this.centre = new Vector2(results[i*3+1], results[i*3+2]);
			}

		}
	
		private void setPixel(int value, int xPos, int yPos) {
			output.img[xPos][yPos][0] = 255;
		}
		
		// draw circle at x y
		public void drawEllipse(int pix, int x0, int y0){
			for(int i=0; i<this.output.getHeight(); i++)
			{
				for(int j=0; j<this.output.getWidth();j++)
				{
					float val = j - x0;
					float val2 = i - y0;
					val = val/this.r;
					val2 = val2/this.r1;
					float ratio = (val*val+val2*val2);
					float margin = 0.01f;
					if(ratio<= 1 + margin && ratio>= 1-margin){
						this.output.img[i][j][0] = 255;
						this.EllipseOnly.img[i][j][0] = 255;
					}
					
				}
			}
			
			
		}
		
		public ImagePerso decoupeEllipse(ImagePerso img){
			ImagePerso imgReturn = new ImagePerso(img);
			for(int i=0; i<img.getHeight(); i++)
			{
				for(int j=0; j<img.getWidth();j++)
				{
					float val = j - this.centre.x;
					float val2 = i - this.centre.y;
					val = val/this.r;
					val2 = val2/this.r1;
					float ratio = (val*val+val2*val2);
					if(ratio <= 1){
						imgReturn.img[i][j][0] =img.img[i][j][0];
						imgReturn.img[i][j][1] =img.img[i][j][1];
						imgReturn.img[i][j][2] =img.img[i][j][2];
					}
					else
					{
						imgReturn.img[i][j][0] =0;
						imgReturn.img[i][j][1] =0;
						imgReturn.img[i][j][2] =0;
					}
					
				}
			}
			return imgReturn;
		}
		
		
	

	public int[] getAcc() {
		return acc;
	}


	}