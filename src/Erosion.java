
public class Erosion extends Filtre{

	
	public Erosion(){
		int Gx[][] = {{0,1,0} , {1,1,1} , {0,1,0} };
		this.noyaux.add(new NoyauConvolution(3,1,Gx));
	}
	
	@Override
	public ImagePerso apply(ImagePerso img){
		
		ImagePerso newImage = new ImagePerso(img.getHeight(),img.getWidth());
		newImage.original = img.original;
		int taille_noyau = this.noyaux.get(0).taille;
		int marge = taille_noyau/2;
		double tmpSum;
		
		for(int i=marge ; i<img.getHeight()-marge; i++){ //pour chaque pixel de l'image 
			for(int j=marge; j <img.getWidth()-marge ; j++){  //pour chaque pixel de l'image 
				
				double sum = 0 ;		
				int ligne =0; 
				int colonne =0;
				
				for(int o = i-marge ; o <i-marge+taille_noyau ; o++){ //on se déplace dans le noyau sur l'image
					for(int p = j-marge; p<j-marge+taille_noyau; p++){
						tmpSum = img.img[o][p][0];		
						
						double multi = this.noyaux.get(0).noyau[ligne][colonne];   //tab[lignes][colonnes]

						if(multi == 1 && tmpSum == 255){
							sum++;
						}
					
						
						colonne += 1;
						if(colonne >= taille_noyau){
							ligne +=1;
							colonne =0;
						}
						if(ligne >= taille_noyau)ligne =0;
						
						
					}
				} //din déplacement dans le noyau
				
				if(sum == 5){       //mettre == 5 permet de vider une forme et garder que le contour
					newImage.img[i][j][0] = 0;
					newImage.img[i][j][1] = 0;
					newImage.img[i][j][2] = 0;
				}
				else
				{
					newImage.img[i][j][0] = img.img[i][j][0];
					newImage.img[i][j][1] = img.img[i][j][1];
					newImage.img[i][j][2] = img.img[i][j][2];
				}

			}
		}
		
		for(int i=marge ; i<img.getHeight()-marge; i++){ //pour chaque pixel de l'image 
			for(int j=marge; j <img.getWidth()-marge ; j++){
				if(img.img[i][j][0] == newImage.img[i][j][0] && img.img[i][j][0] == 255){
					newImage.img[i][j][0]=0;
					newImage.img[i][j][1]=0;
					newImage.img[i][j][2]=0;
				}
				else 
				{
					if(img.img[i][j][0] == 255 && newImage.img[i][j][0] == 0){
						newImage.img[i][j][0]=255;
						newImage.img[i][j][1]=255;
						newImage.img[i][j][2]=255;
					}
				}
			}
		}
		
		return newImage;
	}
	
	public ImagePerso erosionMultiple(ImagePerso image, int valeur){
		
		ImagePerso newImage = new ImagePerso(image.getHeight(),image.getWidth());
		newImage.original = image.original;
		
		newImage = this.apply(image);
		for(int i=0 ; i< valeur; i++){
			newImage = this.apply(newImage);
		}
		
		return newImage;
	}
}


