
public class Squelette extends Filtre{

	
	public Squelette(){

	}
	
	@Override
	public ImagePerso apply(ImagePerso img){
		
		ImagePerso newImage = new ImagePerso(img.getHeight(),img.getWidth());
		newImage.original = img.original;
		Boolean[][] tabBool = new Boolean[img.getHeight()][img.getWidth()] ;
		
		for(int i=0 ; i<img.getHeight(); i++){
			for(int j=0; j<img.getWidth(); j++){
				tabBool[i][j] = false; //initialisation du tableau a false
			}
		}

		Boolean test = true;
		
/*-- ------------------------------------ PASSE 1 ---------------------------------- */
		for(int i=1 ; i<img.getHeight()-1; i++){ //pour chaque pixel de l'image 
			for(int j=1; j <img.getWidth()-1 ; j++){  //pour chaque pixel de l'image 
				
				 test = true;
				if(!(img.img[i][j][0] != 0)) //si le pixel est d�j� noir
				{
					test = false;
				}
				
				
				if(!(nbrVoisins(img,i,j) >=2)){  //garde un squelette de taille2
					test = false;
				}
				
				if(!(nbrVoisins(img,i,j) <=6)){  //assure bordure d'un masque 3x3
					test = false;
				}
				
				/*-------------- TABLEAU POUR CHANGEMENT VOISINAGE --------------- */
				Boolean[][] voisins = new Boolean[3][3];
				for(int i1=0; i1<3 ; i1++){
					for(int j1=0 ; j1<3; j1++){
						voisins[i1][j1] = false;
					}
				}
				int tmpI = 1;
				int tmpJ = 1;
				if(img.img[i][j+1][0] == 0)voisins[tmpI][tmpJ+1]=true;
				if(img.img[i+1][j+1][0] == 0)voisins[tmpI+1][tmpJ+1]=true;
				if(img.img[i+1][j][0] == 0)voisins[tmpI+1][tmpJ]=true;
				if(img.img[i+1][j-1][0] == 0)voisins[tmpI+1][tmpJ-1]=true;
				if(img.img[i][j-1][0] == 0)voisins[tmpI][tmpJ-1]=true;
				if(img.img[i-1][j-1][0] == 0)voisins[tmpI-1][tmpJ-1]=true;
				if(img.img[i-1][j][0] == 0)voisins[tmpI-1][tmpJ]=true;
				if(img.img[i-1][j+1][0] == 0)voisins[tmpI-1][tmpJ+1]=true;
				
				/*-------------- FIN TABLEAU POUR CHANGEMENT VOISINAGE --------------- */
				
				if(!(nbrChangement(voisins,1,1) ==1)){  //nombre changement des voisins false-true ==1 (evite fragmentation)
					test = false;
				}
				
				
				if(!(voisins[tmpI][tmpJ+1] || voisins[tmpI+1][tmpJ] || voisins[tmpI][tmpJ-1])== true){  //si v[0] || v[2] || v[4] == true
					test = false;
				}
				
				if(!(voisins[tmpI+1][tmpJ] || voisins[tmpI][tmpJ-1] || voisins[tmpI-1][tmpJ])== true){  //si v[0] || v[2] || v[4] == true
					test = false;
				} 
				
				//Si tout les tests sont v�rifi�s on passe a true
				if(test){
					tabBool[i][j] = true;
					//System.out.print("Tous les tests sont pass�s ! GG !");
				}
			}
		}
		

		
		//init image
		for(int i=0 ; i<img.getHeight(); i++){
			for(int j=0; j<img.getWidth(); j++){
					newImage.img[i][j][0] = img.img[i][j][0];
					newImage.img[i][j][1] = img.img[i][j][1];
					newImage.img[i][j][2] = img.img[i][j][2];
				
			}
		}
		
		for(int i=0 ; i<img.getHeight(); i++){
			for(int j=0; j<img.getWidth(); j++){
				if(tabBool[i][j] == true){
					newImage.img[i][j][0] = 0;
					newImage.img[i][j][1] = 0;
					newImage.img[i][j][2] = 0;
				}
			}
		}
		
		for(int i=0 ; i<img.getHeight(); i++){
			for(int j=0; j<img.getWidth(); j++){
				tabBool[i][j] = false; //initialisation du tableau a false
			}
		}
		
		ImagePerso newImage2 = new ImagePerso(img.getHeight(),img.getWidth());
		newImage2.original = newImage.convertFromImage();
		
		for(int i=0 ; i<img.getHeight(); i++){
			for(int j=0; j<img.getWidth(); j++){
					newImage2.img[i][j][0] = newImage.img[i][j][0];
					newImage2.img[i][j][1] = newImage.img[i][j][1];
					newImage2.img[i][j][2] = newImage.img[i][j][2];
				
			}
		}
		
/*---- --------------------------- PASSE 2 ---------------------------------- */
		for(int i=0 ; i<img.getHeight(); i++){
			for(int j=0; j<img.getWidth(); j++){
				tabBool[i][j] = false; //initialisation du tableau a false
			}
		}

		test = true;
		
		
		for(int i=1 ; i<img.getHeight()-1; i++){ //pour chaque pixel de l'image 
			for(int j=1; j <img.getWidth()-1 ; j++){  //pour chaque pixel de l'image 
				
				 test = true;
				if(!(img.img[i][j][0] != 0)) //si le pixel est d�j� noir
				{
					test = false;
				}
				
				
				if(!(nbrVoisins(img,i,j) >=2)){  //garde un squelette de taille2
					test = false;
				}
				
				if(!(nbrVoisins(img,i,j) <=6)){  //assure bordure d'un masque 3x3
					test = false;
				}
				
				/*-------------- TABLEAU POUR CHANGEMENT VOISINAGE --------------- */
				Boolean[][] voisins = new Boolean[3][3];
				for(int i1=0; i1<3 ; i1++){
					for(int j1=0 ; j1<3; j1++){
						voisins[i1][j1] = false;
					}
				}
				int tmpI = 1;
				int tmpJ = 1;
				if(newImage.img[i][j+1][0] == 0)voisins[tmpI][tmpJ+1]=true;
				if(newImage.img[i+1][j+1][0] == 0)voisins[tmpI+1][tmpJ+1]=true;
				if(newImage.img[i+1][j][0] == 0)voisins[tmpI+1][tmpJ]=true;
				if(newImage.img[i+1][j-1][0] == 0)voisins[tmpI+1][tmpJ-1]=true;
				if(newImage.img[i][j-1][0] == 0)voisins[tmpI][tmpJ-1]=true;
				if(newImage.img[i-1][j-1][0] == 0)voisins[tmpI-1][tmpJ-1]=true;
				if(newImage.img[i-1][j][0] == 0)voisins[tmpI-1][tmpJ]=true;
				if(newImage.img[i-1][j+1][0] == 0)voisins[tmpI-1][tmpJ+1]=true;
				
				/*-------------- FIN TABLEAU POUR CHANGEMENT VOISINAGE --------------- */
				
				if(!(nbrChangement(voisins,1,1) ==1)){  //nombre changement des voisins false-true ==1 (evite fragmentation)
					test = false;
				}
				
				
				if(!(voisins[tmpI][tmpJ+1] || voisins[tmpI+1][tmpJ] || voisins[tmpI-1][tmpJ])== true){  //si v[0] || v[2] || v[4] == true
					test = false;
				}
				
				if(!(voisins[tmpI][tmpJ+1] || voisins[tmpI][tmpJ-1] || voisins[tmpI-1][tmpJ])== true){  //si v[0] || v[2] || v[4] == true
					test = false;
				} 
				
				//Si tout les tests sont v�rifi�s on passe a true
				if(test){
					tabBool[i][j] = true;
					//System.out.print("Tous les tests sont pass�s ! GG !");
				}
			}
		}
		
		//init image
				for(int i=0 ; i<img.getHeight(); i++){
					for(int j=0; j<img.getWidth(); j++){
							newImage2.img[i][j][0] = img.img[i][j][0];
							newImage2.img[i][j][1] = img.img[i][j][1];
							newImage2.img[i][j][2] = img.img[i][j][2];
						
					}
				}
				
				for(int i=0 ; i<img.getHeight(); i++){
					for(int j=0; j<img.getWidth(); j++){
						if(tabBool[i][j] == true){
							newImage2.img[i][j][0] = 0;
							newImage2.img[i][j][1] = 0;
							newImage2.img[i][j][2] = 0;
						}
					}
				}
		
		return newImage2;
	}
	
	public int nbrVoisins(ImagePerso img, int x, int y){
		int sum =0;
		if(img.img[x][y+1][0] == 255)sum++;
		if(img.img[x+1][y+1][0] == 255)sum++;
		if(img.img[x+1][y][0] == 255)sum++;
		if(img.img[x+1][y-1][0] == 255)sum++;
		if(img.img[x][y-1][0] == 255)sum++;
		if(img.img[x-1][y-1][0] == 255)sum++;
		if(img.img[x-1][y][0] == 255)sum++;
		if(img.img[x-1][y+1][0] == 255)sum++;
		return sum;
	}
	
	public int nbrChangement(Boolean[][] tab, int x , int y){
		int sum =0;
		if(tab[x][y+1] == false && tab[x+1][y+1] == true)sum++;
		if(tab[x+1][y+1] == false && tab[x+1][y] ==true)sum++;
		if(tab[x+1][y] == false && tab[x+1][y-1] ==true)sum++;
		if(tab[x+1][y-1] == false && tab[x][y-1] ==true)sum++;
		if(tab[x][y-1] == false && tab[x-1][y-1] ==true)sum++;
		if(tab[x-1][y-1] == false && tab[x-1][y] ==true )sum++;
		if(tab[x-1][y] == false && tab[x-1][y+1] ==true)sum++;
		if(tab[x-1][y+1] == false && tab[x][y+1] ==true)sum++;
		//System.out.println("SUM : " +sum);
		return sum;
	}
	
}


