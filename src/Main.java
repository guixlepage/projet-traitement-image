import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Sample.fxml"));
        primaryStage.setTitle("Projet Traitement Image");
        primaryStage.setScene(new Scene(root, 1000, 565));
        //primaryStage.setMaximized(true);
        primaryStage.show();
        
        
    }

    public static void main(String[] args) {
        launch(args);
    }

}
