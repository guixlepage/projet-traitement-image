import java.awt.Color;
import java.util.ArrayList;

public class Growing {
	ImagePerso imgOrigin;
	ImagePerso imgLuminance;
	int seedX,seedY;
	ArrayList<Vector2> pixels = new ArrayList<Vector2>();
	int threshold;
	double intensiteDepart;
	
	public Growing(ImagePerso img, int x, int y, int threshold){
		this.imgOrigin = new ImagePerso(img);
		this.seedX = x;
		this.seedY = y;
		this.threshold = threshold;
		
		this.imgLuminance = new ImagePerso(img);
		
		this.imgLuminance.luminance();
		this.pixels.add(new Vector2(this.seedX,this.seedY));
		this.intensiteDepart = this.imgLuminance.img[this.seedX][this.seedY][0];

		ArrayList<Vector2> pixelAVoir = new ArrayList<Vector2>();
		Boolean[][] tabBool = new Boolean[this.imgOrigin.getHeight()][this.imgOrigin.getWidth()];
		for(int i=0; i<this.imgOrigin.getHeight()-1; i++){
			for(int j=0 ; j<this.imgOrigin.getWidth()-1; j++){
				tabBool[i][j] = false;
			}
		}
		tabBool[this.seedX][this.seedY] = true; 
		addVoisin(pixelAVoir,tabBool, this.seedX, this.seedY);
		
		do
		{
			for(int i=0; i<pixelAVoir.size(); i++){
				if((this.imgLuminance.img[pixelAVoir.get(i).x][pixelAVoir.get(i).y][0] > this.intensiteDepart - this.threshold ) && (this.imgLuminance.img[pixelAVoir.get(i).x][pixelAVoir.get(i).y][0] < this.intensiteDepart + this.threshold ))
				{
					//on garde
					Vector2 pixelTmp = new Vector2(pixelAVoir.get(i).x , pixelAVoir.get(i).y);
					this.pixels.add(pixelTmp);	
					if(pixelAVoir.get(i).x > 1 && pixelAVoir.get(i).y > 1 && pixelAVoir.get(i).x < this.imgLuminance.getHeight()-2 && pixelAVoir.get(i).y < this.imgLuminance.getWidth()-2)
					{
						addVoisin(pixelAVoir , tabBool, pixelAVoir.get(i).x , pixelAVoir.get(i).y);
					}
					pixelAVoir.remove(i);
				}
				else
				{
					pixelAVoir.remove(i);
				}			
			}
		}while(!pixelAVoir.isEmpty());
		
	}
	
	public ImagePerso colorSelectZone(Color couleur){
		for(int i=0; i<this.pixels.size(); i++){
			this.imgOrigin.img[this.pixels.get(i).x][this.pixels.get(i).y][0] = couleur.getRed();
			this.imgOrigin.img[this.pixels.get(i).x][this.pixels.get(i).y][1] = couleur.getGreen();
			this.imgOrigin.img[this.pixels.get(i).x][this.pixels.get(i).y][2] = couleur.getBlue();
		}
		
		return this.imgOrigin;
	}
	
	public void addVoisin(ArrayList<Vector2> tmp, Boolean[][] boolTab, int x, int y){
			if(boolTab[x-1][y-1] == false)
			{
				tmp.add(new Vector2(x-1,y-1));
				boolTab[x-1][y-1] = true;
			}
			if(boolTab[x-1][y] == false)
			{
				tmp.add(new Vector2(x-1,y));
				boolTab[x-1][y] = true;
			}
			if(boolTab[x-1][y+1] == false)
			{
				tmp.add(new Vector2(x-1,y+1));
				boolTab[x-1][y+1] = true;
			}
			if(boolTab[x][y-1] == false)
			{
				tmp.add(new Vector2(x,y-1));
				boolTab[x][y-1] = true;
			}
			if(boolTab[x][y+1] == false)
			{
				tmp.add(new Vector2(x,y+1));
				boolTab[x][y+1] = true;
			}
			if(boolTab[x+1][y-1] == false)
			{
				tmp.add(new Vector2(x+1,y-1));
				boolTab[x+1][y-1] = true;
			}
			if(boolTab[x+1][y] == false)
			{
				tmp.add(new Vector2(x+1,y));
				boolTab[x+1][y] = true;
			}
			if(boolTab[x+1][y+1] == false)
			{
				tmp.add(new Vector2(x+1,y+1));
				boolTab[x+1][y+1] = true;
			}
	}
		
}
	

