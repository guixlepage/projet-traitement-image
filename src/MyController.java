import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.function.UnaryOperator;

import javax.imageio.ImageIO;
import javax.swing.event.ChangeListener;

import org.controlsfx.control.Notifications;

import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.converter.IntegerStringConverter;

public class MyController {
	
	/*--- GUI DECLARATION -----*/
	Pane pane;

	Stage stage;
	Button OpenButton;
	Button arriere;
	
	/*-------------------------*/
	ImagePerso imgPerso;
	ImagePerso sauvEllipse;
	ImagePerso temp;
	ImagePerso temp2;
	BufferedImage imageOrigin;
	BufferedImage imageDisplay;
	Image displayNow;
	
	/*---- Flechette ------ */ 
	Vector2 centreEllipse;
	
	@FXML
	private TextField nbrIterations;
	
	@FXML
	private Stage primaryStage;
	
	@FXML
	private Slider majorThreshold;
	
	@FXML
	private Slider ThresholdEllipse;
	
	@FXML
	private Slider minorThreshold;
	
	@FXML
	private ImageView displayImage;
	
	@FXML
	private Pane transfoPannel;
	
	@FXML
	private Pane menuGauche;
	
	@FXML
	private Pane squelettePane;
	
	@FXML
	private Pane ellipse;
	
	@FXML
	private Pane menuNoyau;
	
	@FXML
	private Pane growing;
	
	@FXML
	private TextField threshold;
	
	@FXML
	private TextField sizeMatrice;
	
	public MyController(){
		//this.displayImage = new ImageView();
	}
	
@FXML
    void onOpenClick(ActionEvent event) {
    		
      	 //FILECHOOSER open
          FileChooser fileChooser = new FileChooser();
          fileChooser.setTitle("Open Image File");
          fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg"));
          
          String currentDir = System.getProperty("user.home");
          File defaultDirectory = new File(currentDir);  
          fileChooser.setInitialDirectory(new File(System.getProperty("user.home") + "\\Pictures")); // fileChooser.setInitialDirectory(new File(System.getProperty("user.home") + "\\Pictures")); ou fileChooser.setInitialDirectory(defaultDirectory);
          File selected = fileChooser.showOpenDialog(stage);
          
          if (selected != null) {
              System.out.println(selected.getAbsolutePath()); // Affichage du path de l'image s�l�ctionn�
              try {        	   
            	  displayNow = new Image(selected.toURI().toString()); // "file://"+selected.getAbsolutePath() ou //selected.toURI().toURL().toExternalForm()  
            	   this.imageOrigin = javax.imageio.ImageIO.read(selected);
            	   this.imageDisplay = this.imageOrigin;
            	   Image newImg = SwingFXUtils.toFXImage(this.imageDisplay, null);
                   this.displayImage.setImage(newImg);
                   
                   //on initialise notre class image PERSO 
                   BufferedImage image = SwingFXUtils.fromFXImage(this.displayImage.getImage(), null);
                  // this.imgPerso = new ImagePerso(image);
                   this.imgPerso = new ImagePerso(this.imageOrigin);
                   this.imgPerso.original = this.imageOrigin;
              		
              } catch (Exception e) {
                  e.printStackTrace();
              }
          } 
         // this.displayImage.setFitHeight(this.imgPerso.getHeight());
         // this.displayImage.setFitWidth(this.imgPerso.getWidth());
    }

@FXML
  void onSaveClick(ActionEvent event) {
	FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Resource File");
    fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg"));
    String currentDir = System.getProperty("user.home");
    File defaultDirectory = new File(currentDir);
    fileChooser.setInitialDirectory(defaultDirectory);
    File selected = fileChooser.showSaveDialog(stage);
    if (selected != null) {
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(this.displayImage.getImage(), null), "png", selected); //enregistrer le fichier
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    }

@FXML
void Luminance(ActionEvent event) {
	this.imgPerso.luminance();
	this.displayImage.setImage(SwingFXUtils.toFXImage(this.imgPerso.convertFromImage(), null));
}

@FXML
void Sobel(ActionEvent event) {
	Sobel sob = new Sobel();
	//image = sob.applyBrut(image);  //application direct et brut pour Sobel
	this.imgPerso = sob.apply(this.imgPerso); //application avec prise en compte taille filtre
	this.displayImage.setImage(SwingFXUtils.toFXImage(this.imgPerso.convertFromImage(), null));
}

@FXML
void Seuillage(ActionEvent event){
	this.imgPerso.seuillage();
	this.displayImage.setImage(SwingFXUtils.toFXImage(this.imgPerso.convertFromImage(), null));
}

@FXML
void detectLine(){
	lineHough lineDetect = new lineHough();
	lineDetect.init(this.imgPerso, this.imgPerso.getWidth(), this.imgPerso.getHeight());
	this.imgPerso = lineDetect.process();
	this.displayImage.setImage(SwingFXUtils.toFXImage(this.imgPerso.convertFromImage(), null));
	//lineDetect.getCentreMoyen();
}

@FXML
void Erosion(ActionEvent event){
	Erosion ero = new Erosion();
	this.imgPerso = ero.apply(this.imgPerso);
	this.displayImage.setImage(SwingFXUtils.toFXImage(this.imgPerso.convertFromImage(), null));
}

@FXML
void Dilatation(ActionEvent event){
	Dilatation dil = new Dilatation();
	this.imgPerso= dil.apply(this.imgPerso);
	this.displayImage.setImage(SwingFXUtils.toFXImage(this.imgPerso.convertFromImage(), null));
}
 
@FXML
void Amincissement(ActionEvent event){
	this.menuGauche.managedProperty().bind(menuGauche.visibleProperty());
	this.menuGauche.setVisible(false);
	this.ellipse.managedProperty().bind(ellipse.visibleProperty());
	this.ellipse.setVisible(false);
	this.growing.managedProperty().bind(growing.visibleProperty());
	this.growing.setVisible(false);
	this.transfoPannel.managedProperty().bind(ellipse.visibleProperty());
	this.transfoPannel.setVisible(false);
	this.squelettePane.setVisible(true);
	
}

@FXML
void squeletteValidate(){
	int iteration = 1;
	try{
		iteration = Integer.parseInt(this.nbrIterations.getText());
	 } catch(NumberFormatException e){
		 Notifications.create()
	     .title("ERREUR!")
	     .text("Saisissez un nombre! ")
	     .showWarning(); 
	 }
	
	Squelette skull = new Squelette();
	for(int i =0 ; i <iteration; i++){
		this.imgPerso= skull.apply(this.imgPerso);
	}
	this.displayImage.setImage(SwingFXUtils.toFXImage(this.imgPerso.convertFromImage(), null));
}

@FXML
void Growing(ActionEvent event)throws Exception{
	this.menuGauche.managedProperty().bind(menuGauche.visibleProperty());
	this.menuGauche.setVisible(false);
	this.ellipse.managedProperty().bind(ellipse.visibleProperty());
	this.ellipse.setVisible(false);
	this.squelettePane.managedProperty().bind(squelettePane.visibleProperty());
	this.squelettePane.setVisible(false);
	this.transfoPannel.managedProperty().bind(ellipse.visibleProperty());
	this.transfoPannel.setVisible(false);
	this.growing.setVisible(true);
	
	UnaryOperator<Change> integerFilter = change -> {
	    String newText = change.getControlNewText();
	    if (newText.matches("-?([1-9][0-9]*)?")) { 
	        return change;
	    }
	    return null;
	};
	this.threshold.setTextFormatter(
		    new TextFormatter<Integer>(new IntegerStringConverter(), 10, integerFilter));
}

@FXML
void selectPixel(ActionEvent event){
	
		this.displayImage.setPickOnBounds(true);
		
		this.displayImage.setOnMouseClicked((e -> {
			int threshold = Integer.parseInt(this.threshold.getText());
			if(threshold > 255)threshold =255;
			if(threshold < 0)threshold =0;
		        System.out.println("["+e.getY()+", "+e.getX()+"]");
		        int x =(int) e.getY();
		        int y =(int) e.getX();
				appelGrowing(this.imgPerso, x, y, threshold);
				this.displayImage.setOnMouseClicked(null);
	     }));		
		

}

void appelGrowing(ImagePerso img, int x, int y, int threshold){
	// 500 x 391
	System.out.println("Imageview x: "+ this.displayImage.getFitHeight() +" y "+this.displayImage.getFitWidth());
	System.out.println("L'image fait : "+ this.imgPerso.getHeight() + ","+ this.imgPerso.getWidth());
	System.out.println(" X: "+ x + " Y: " + y );
	x = (int) ((x *this.imgPerso.height)/ this.displayImage.getFitHeight());
	y = (int) ((y *this.imgPerso.width)/ this.displayImage.getFitWidth());

	System.out.println("Scale de XY : "+ x +" , "+ y );
	Growing grow = new Growing(this.imgPerso, x, y, threshold);
	this.imgPerso = grow.colorSelectZone(Color.RED);
	this.displayImage.setImage(SwingFXUtils.toFXImage(this.imgPerso.convertFromImage(), null));
}
  
@FXML
void ellipseDetection(){
	this.menuGauche.managedProperty().bind(menuGauche.visibleProperty());
	this.menuGauche.setVisible(false);
	this.growing.managedProperty().bind(growing.visibleProperty());
	this.growing.setVisible(false);
	this.squelettePane.managedProperty().bind(squelettePane.visibleProperty());
	this.squelettePane.setVisible(false);
	this.transfoPannel.managedProperty().bind(ellipse.visibleProperty());
	this.transfoPannel.setVisible(false);
	this.ellipse.setVisible(true);
	this.majorThreshold.setMax(this.imageOrigin.getHeight());
	this.minorThreshold.setMax(this.imageOrigin.getWidth());
	this.sauvEllipse = new ImagePerso(this.imgPerso);
}

 @FXML 
 void ellipseValidate(){
	 	this.imgPerso = this.sauvEllipse;
	 	this.displayImage.setImage(SwingFXUtils.toFXImage(this.imgPerso.convertFromImage(), null));
		int major = (int)this.majorThreshold.getValue();
		
		int minor = (int)this.minorThreshold.getValue();
		ellipseHough elliDetect = new ellipseHough();
		//public void init(int[] inputIn, int widthIn, int heightIn, int radius, int radius1)
		elliDetect.init(this.imgPerso,  this.imgPerso.width, this.imgPerso.height, minor, major);
		this.imgPerso = elliDetect.process();
		this.centreEllipse = elliDetect.centre;

		this.temp2 = elliDetect.decoupeEllipse(new ImagePerso(this.imageOrigin));
		this.displayImage.setImage(SwingFXUtils.toFXImage(this.imgPerso.convertFromImage(), null));
 }
 
 @FXML 
 void cutEllipse(){
	 this.imgPerso = this.temp2;
	 this.displayImage.setImage(SwingFXUtils.toFXImage(this.imgPerso.convertFromImage(), null));
 }

 
 @FXML
 void transformation(ActionEvent event) {
	 	this.menuGauche.managedProperty().bind(menuGauche.visibleProperty());
		this.menuGauche.setVisible(false);
		this.growing.managedProperty().bind(growing.visibleProperty());
		this.growing.setVisible(false);
		this.squelettePane.managedProperty().bind(squelettePane.visibleProperty());
		this.squelettePane.setVisible(false);
		this.ellipse.managedProperty().bind(ellipse.visibleProperty());
		this.ellipse.setVisible(false);
		this.transfoPannel.setVisible(true);
 }
 
 @FXML
 void verifCibleDroite(){
	Sobel sob = new Sobel();
	this.imgPerso = sob.apply(this.imgPerso);
	this.imgPerso.seuillage();
	lineHough lineDetect = new lineHough();
	lineDetect.init(this.imgPerso, this.imgPerso.getWidth(), this.imgPerso.getHeight());
	this.imgPerso = lineDetect.process();
	this.displayImage.setImage(SwingFXUtils.toFXImage(this.imgPerso.convertFromImage(), null));
	System.out.println("Centre ellipse " + this.centreEllipse.x +";"+this.centreEllipse.y);
	System.out.println("Centre droite "+ lineDetect.pointIntersectionMulti.x + ";"+ lineDetect.pointIntersectionMulti.y);
	double distanceEntreCentre = Math.sqrt(Math.pow((this.centreEllipse.x-lineDetect.pointIntersectionMulti.x), 2)+Math.pow((this.centreEllipse.y-lineDetect.pointIntersectionMulti.y), 2));
	System.out.println("La distance entre les deux centres est de: "+distanceEntreCentre);
	if(distanceEntreCentre < 15){
		Alert detectCheck = new Alert(Alert.AlertType.INFORMATION);
		detectCheck.setTitle("Cible trouv�e!");
		detectCheck.setHeaderText("Vous avez trouv� une cible donc le centre est (x,y) :" + this.centreEllipse.x +" , "+ this.centreEllipse.y);
		detectCheck.show();
	}
 }
 
 
 @FXML
 void SetUpNoyau(){
	 this.menuGauche.managedProperty().bind(menuGauche.visibleProperty());
		this.menuGauche.setVisible(false);
		this.growing.managedProperty().bind(growing.visibleProperty());
		this.growing.setVisible(false);
		this.squelettePane.managedProperty().bind(squelettePane.visibleProperty());
		this.squelettePane.setVisible(false);
		this.ellipse.managedProperty().bind(ellipse.visibleProperty());
		this.ellipse.setVisible(false);
		this.transfoPannel.managedProperty().bind(transfoPannel.visibleProperty());
		this.transfoPannel.setVisible(false);
		
		this.menuNoyau.setVisible(true);
 }
 
 @FXML
 void valideSize(){
	 int SIZE=3;
	try{
		 SIZE = Integer.parseInt(this.sizeMatrice.getText()); 
	 } catch(NumberFormatException e){
		 Notifications.create()
	     .title("ERREUR!")
	     .text("Saisissez un nombre! ")
	     .showWarning(); 
	 }
	 
	 if(SIZE %2 ==0){
		 Notifications.create()
	     .title("Nombre Impair!")
	     .text("Valeur augment�e de 1!")
	     .showWarning();
		 SIZE ++;
	
	 }
	 


  
        

 }
 

 
}
